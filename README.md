# Signal adapter for hubot
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://tldrlegal.com/license/mit-license#summary)
[![Build Status](https://travis-ci.org/agileek/hubot-signal.svg?branch=master)](https://travis-ci.org/agileek/hubot-signal)
